I made this after trying to endure the pain in the ass that is trying to run a modded Arma 3 Dedicated Server on Linux.

The main source of pain was the fact that, unlike NTFS, ext4 is case-sensitive, and since Bohemia aren't very good at coding, the server assumes lowercase on all file and folder names. So, for example, when it tries to read AddOns/CUP, it instead tries to read addons/cup, and since that folder doesn't exist, it launches without the mod. Very annoying.

So, since I had a mission to run that needed a DS, I made this program that, once given the server executable, Workshop folder, and an empty folder to copy lowercase-ified mods into, gives you a list of all mods in your Workshop folder, and lets you copy them to the server's mods folder with all uppercase characters replaced by lowercase ones. It also parses HTML presets, making it way easier to change what mods are installed on the server. It also has a button to re-install the mods, which clears the mods folder and re-copies all the currently loaded mods to their lowercase forms, which is incredibly useful for updating mods. Way easier (and faster) than doing it by hand.

Evident from the functionless Missions and Config tabs is the fact that I also intended to have it allow you to edit the server's config file and even change mission rotation through the GUI, but I never ended up writing the code to make it actually work due to a lack of time or need. So you'll still have to do that through a text editor or some other program instead.



-----HOW TO RUN IT-----:
Make sure you have Mono installed. Then, cd into GargsA3ServerManager/bin/Debug/ and run:
> mono GargsA3ServerManager.exe

That's it.

If the program crashes, the server will not be closed with it. You can then close the server via opening a System Monitor of sorts and killing the server task. You can also start and immediately stop the server in order to get the terminal command the program used to launch the server - make sure you cd into the server executable's folder first, though, otherwise the server will exit with an odd error that tells you nothing about how to fix it. Which is, sadly, an issue that's very common for anything Bohemia.

For better stability on the GUI, however, I recommend using MonoDevelop (a coding IDE similar to Visual Studio) and hitting the Run button on the top left, which should look like a play button if you're using a default GTK theme. Having the IDE attached to the program seems to make it not crash for whatever reason. I have no idea why it is the way it is, but alas, it is.



Feel free to finish the program, as I probably won't. Not anytime soon, at least. Just make sure to credit me (AlphaGarg) if you do.
