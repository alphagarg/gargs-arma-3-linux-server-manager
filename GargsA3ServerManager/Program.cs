﻿using Gtk;
using System.Threading.Tasks;

class MainClass
{
    //[System.Runtime.InteropServices.DllImport("libX11.so.6")]
    //static extern int XInitThreads();
    static MainWindow mw;
    public static void Main(string[] args)
    {
        //XInitThreads();

        Application.Init();

        mw = new MainWindow();
        /*Task t = new Task(async () =>
        {
            System.Console.WriteLine("Hi :D");
            while (mw != null)
            {
                Application.Invoke(delegate
                {
                    mw.guisc.Buffer.Text = mw.guisco;
                    if (mw.guisc.Buffer.Text.Length > ushort.MaxValue)
                        mw.guisc.Buffer.Text = mw.guisc.Buffer.Text.Remove(0, ushort.MaxValue / 2);
                    else
                    {
                        var v = ((ScrolledWindow)mw.guisc.Parent).Vadjustment;
                        v.Value = v.Upper - v.PageSize * 0.5;
                    }
                });
                await Task.Delay(20);
            }
        });*/
        mw.Title = "Garg's Arma 3 Linux Server Manager";
        mw.Show();

        StartUpdateLoop();

        Application.Run();
    }
    static async void StartUpdateLoop()
    {
        while (mw != null)
        {
            Update();
            await Task.Delay(500);
        }
    }
    static string lastguisco;
    static async void Update()
    {
        if (lastguisco != mw.guisco)
        {
            lastguisco = mw.guisco;
            if (mw.guisco.Length > ushort.MaxValue)
                mw.guisco = mw.guisco.Remove(0, ushort.MaxValue / 2);
            Application.Invoke(delegate
            {
                mw.guisc.Buffer.Text = mw.guisco;

                var v = ((ScrolledWindow)mw.guisc.Parent).Vadjustment;
                v.Value = v.Upper - v.PageSize;
            });

            await Task.Delay(20);
            Application.Invoke(delegate
            {
                var v = ((ScrolledWindow)mw.guisc.Parent).Vadjustment;
                v.Value = v.Upper - v.PageSize;
            });
        }
    }
}
