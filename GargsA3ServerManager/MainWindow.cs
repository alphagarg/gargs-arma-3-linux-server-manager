﻿using System;
using System.IO;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using Gtk;
public partial class MainWindow : Gtk.Window
{
    [Serializable]
    public struct GASMData
    {
        public string serverPath;
        public string workshopPath;
        public string modsPath;
        public string[] mods;
    }
    public void LoadLast(object sender, EventArgs e)
    {
        if (File.Exists(dataPath))
        {
            using (var fs = File.Open(dataPath, FileMode.Open))
            {
                var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                data = (GASMData)binaryFormatter.Deserialize(fs);

                modsLoaded = data.mods.ToList();
            }
            Console.WriteLine("Server Folder: " + data.serverPath);
            Console.WriteLine("Workshop Path: " + data.workshopPath);
            Console.WriteLine("Mods Folder: " + data.modsPath);
            ConfigParse();
            WorkshopUpdate();
            ModsUpdate();
        }
    }

    public GASMData data;
    public List<string> modsLoaded = new List<string>();
    public Dictionary<string, string> modsLoadedNames = new Dictionary<string, string>();
    public static string rootPath
    {
        get => System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
    }
    public static string dataPath
    {
        get => rootPath + "/data.gasm";
    }

    public MainWindow() : base(WindowType.Toplevel) { Build(); }
    protected void OnDeleteEvent(object sender, DeleteEventArgs a)
    {
        FileQuit(null, null);
        a.RetVal = true;
    }
    protected override void OnAdded(Widget widget)
    {
        base.OnAdded(widget);

        LoadLast(null, null);
    }
    protected void FileQuit(object sender, EventArgs e)
    {
        if (server != null && !server.HasExited)
        {
            var md = new MessageDialog(this, DialogFlags.DestroyWithParent, MessageType.Error, ButtonsType.None, "Please shut down the server before exiting this application.", this);
            md.Show();

            return;
        }

        using (var fs = File.Open(dataPath, FileMode.OpenOrCreate))
        {
            data.mods = modsLoaded.ToArray();

            var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            binaryFormatter.Serialize(fs, data);
        }
        Application.Quit();
    }

    protected void FileServerFolder(object sender, EventArgs e)
    {
        FileChooserDialog fcd = new FileChooserDialog("Select Server Executable", null, FileChooserAction.Open);
        fcd.AddButton(Stock.Cancel, ResponseType.Cancel);
        fcd.AddButton(Stock.Open, ResponseType.Ok);
        fcd.DefaultResponse = ResponseType.Ok;
        fcd.SelectMultiple = false;

        ResponseType response = (ResponseType)fcd.Run();
        if (response == ResponseType.Ok)
        {
            data.serverPath = fcd.Filename.Split(new string[] { "/arma3server_x64" }, StringSplitOptions.RemoveEmptyEntries)[0];
            fcd.Destroy();
            ConfigParse();
        }
        else
            fcd.Destroy();
    }
    public void ConfigParse()
    {
        var configPath = data.serverPath + "/server.cfg";
        MessageDialog md;
        if (File.Exists(configPath))
        {
            //@TODO: Parse the server config file and actually do shit with it. Make sure to spawn the message dialog after it's all well and done

            md = new MessageDialog(this, DialogFlags.DestroyWithParent, MessageType.Info, ButtonsType.None, $"Server config at {data.serverPath} successfully loaded!", this);
        }
        else if (!string.IsNullOrEmpty(data.serverPath))
        {
            var fs = File.Create(configPath);

            md = new MessageDialog(this, DialogFlags.DestroyWithParent, MessageType.Info, ButtonsType.None, $"Config file created in {data.serverPath}!", this);
        }
        else
            md = new MessageDialog(this, DialogFlags.DestroyWithParent, MessageType.Error, ButtonsType.None, $"Unable to load server config file at {data.serverPath}", this);
        md.Show();
    }

    protected Dictionary<string, DirectoryInfo> modsFolders;
    protected Dictionary<string, Button> modsButtons = new Dictionary<string, Button>();
    protected void FileWorkshopFolder(object sender, EventArgs e)
    {
        FileChooserDialog fcd = new FileChooserDialog("Select Arma 3 Workshop folder (should be in Steamapps/Workshop/107410)", null, FileChooserAction.SelectFolder);
        fcd.AddButton(Stock.Cancel, ResponseType.Cancel);
        fcd.AddButton(Stock.Open, ResponseType.Ok);
        fcd.DefaultResponse = ResponseType.Ok;
        fcd.SelectMultiple = false;

        ResponseType response = (ResponseType)fcd.Run();
        if (response == ResponseType.Ok)
        {
            data.workshopPath = fcd.Filename;
            fcd.Destroy();
            WorkshopUpdate();
        }
        else
            fcd.Destroy();
        new MessageDialog(this, DialogFlags.DestroyWithParent, MessageType.Info, ButtonsType.None, "List of available mods updated", this).Show();
    }
    public void WorkshopUpdate()
    {
        var workshopDirectoryInfo = new DirectoryInfo(data.workshopPath);
        var modDirs = workshopDirectoryInfo.EnumerateDirectories();
        modsFolders = new Dictionary<string, DirectoryInfo>();
        foreach (var m in modDirs)
        {
            if (!m.Name.Any(x => char.IsLetter(x)))
            {
                var b = new Button { Name = m.Name, Label = m.Name };
                foreach (var f in m.GetFiles())
                {
                    if (b.Label == m.Name)
                    {
                        if (f.Name == "mod.cpp" || f.Name == "meta.cpp")
                        {
                            using (var fs = new FileStream(f.FullName, FileMode.Open))
                            {
                                using (var sr = new StreamReader(fs))
                                {
                                    while (!sr.EndOfStream)
                                    {
                                        var s = sr.ReadLine().Split(new string[] { "\"" }, StringSplitOptions.RemoveEmptyEntries);
                                        if (s.Length > 1 && s[0].Contains("name"))
                                            b.Label = s.Length > 1 ? s[s.Length - 2] : s[0];
                                    }
                                }
                            }
                        }
                    }
                }
                modsLoadedNames.Add(b.Name, b.Label);

                if (modsLoaded.Contains(m.Name))
                {
                    b.Clicked += UnloadMod;
                    ModsLoaded.Add(b);
                }
                else
                {
                    b.Clicked += LoadMod;
                    ModsAvailable.Add(b);
                }
                modsFolders.Add(m.Name, m);
                modsButtons.Add(m.Name, b);

                b.Show();
            }
        }
    }
    public void LoadMod(object sender, EventArgs e)
    {
        var b = (Button)sender;
        b.Clicked -= LoadMod;
        b.Clicked += UnloadMod;
        ModsAvailable.Remove(b);
        ModsLoaded.Add(b);

        modsLoaded.Add(b.Name);
    }
    public void UnloadMod(object sender, EventArgs e)
    {
        var b = (Button)sender;
        b.Clicked -= UnloadMod;
        b.Clicked += LoadMod;
        ModsLoaded.Remove(b);
        ModsAvailable.Add(b);

        modsLoaded.Remove(b.Name);
    }

    protected void FileModsFolder(object sender, EventArgs e)
    {
        FileChooserDialog fcd = new FileChooserDialog("Select an empty folder", null, FileChooserAction.SelectFolder);
        fcd.AddButton(Stock.Cancel, ResponseType.Cancel);
        fcd.AddButton(Stock.Open, ResponseType.Ok);
        fcd.DefaultResponse = ResponseType.Ok;
        fcd.SelectMultiple = false;

        ResponseType response = (ResponseType)fcd.Run();
        if (response == ResponseType.Ok)
            data.modsPath = fcd.Filename;
        fcd.Destroy();
    }

    protected void ModsInstall(object sender, EventArgs e)
    {
        if (data.modsPath == "" || data.modsPath == data.workshopPath)
        {
            new MessageDialog(this, DialogFlags.DestroyWithParent, MessageType.Error, ButtonsType.None, "The mods path is either empty or the same as the Workshop path. Assign it an empty folder!", this).Run();
            return;
        }

        foreach (var m in modsLoaded)
        {
            var md = new MessageDialog(this, DialogFlags.DestroyWithParent, MessageType.Info, ButtonsType.None, $"Installing mod of Workshop ID {m}. This might take a while..", this);
            md.Show();
            CopyFilesRecursively(modsFolders[m], new DirectoryInfo($"{data.modsPath}/{m}"));
            md.Destroy();
        }
        new MessageDialog(this, DialogFlags.DestroyWithParent, MessageType.Info, ButtonsType.None, $"Finished installing {modsLoaded.Count} mods", this).Run();
    }
    protected void ModsReinstall(object sender, EventArgs e)
    {
        if (data.modsPath == "" || data.modsPath == data.workshopPath)
        {
            new MessageDialog(this, DialogFlags.DestroyWithParent, MessageType.Error, ButtonsType.None, "The mods path is either empty or the same as the Workshop path. Assign it an empty folder!", this).Run();
            return;
        }

        foreach (var m in modsLoaded)
        {
            Title = $"(Busy: Installing {modsLoadedNames[m]}) Garg's Arma 3 Linux Server Manager";
            var md = new MessageDialog(this, DialogFlags.DestroyWithParent, MessageType.Info, ButtonsType.None, $"Installing mod of Workshop ID {modsLoadedNames[m]}. This might take a while..", this);
            md.Show();
            var di = new DirectoryInfo(System.IO.Path.Combine(data.modsPath, m));
            di.Delete(true);
            CopyFilesRecursively(modsFolders[m], di);
            md.Destroy();
        }
        Title = "Garg's Arma 3 Linux Server Manager";
        new MessageDialog(this, DialogFlags.DestroyWithParent, MessageType.Info, ButtonsType.None, $"Finished reinstalling {modsLoaded.Count} mods", this).Run();
    }
    protected void CopyFilesRecursively(DirectoryInfo source, DirectoryInfo target, bool checkExists = true)
    {
        if (target.Exists && checkExists)
            return;

        foreach (DirectoryInfo dir in source.GetDirectories())
            CopyFilesRecursively(dir, target.CreateSubdirectory(dir.Name.ToLower()), false);
        foreach (FileInfo file in source.GetFiles())
        {
            var filePath = System.IO.Path.Combine(target.FullName, file.Name.ToLower());
            if (!File.Exists(filePath))
                file.CopyTo(filePath);
        }
    }

    protected void ModsPresetLoad(object sender, EventArgs e)
    {
        FileChooserDialog fcd = new FileChooserDialog("Select Mod Preset (.html)", null, FileChooserAction.Open);
        fcd.AddButton(Stock.Cancel, ResponseType.Cancel);
        fcd.AddButton(Stock.Open, ResponseType.Ok);
        fcd.DefaultResponse = ResponseType.Ok;
        fcd.SelectMultiple = false;

        ResponseType response = (ResponseType)fcd.Run();
        if (response == ResponseType.Ok)
        {
            modsLoaded.Clear();
            foreach (var b in ModsLoaded)
            {
                var b2 = (Button)b;
                ModsLoaded.Remove(b2);
                ModsAvailable.Add(b2);
            }
            using (var fs = File.Open(fcd.Filename, FileMode.Open))
            {
                fcd.Destroy();
                using (var sr = new StreamReader(fs))
                {
                    while (!sr.EndOfStream)
                    {
                        foreach (var s in sr.ReadLine().Split(new string[] { "http://steamcommunity.com/sharedfiles/filedetails/?id=", "\"" }, StringSplitOptions.RemoveEmptyEntries))
                        {
                            if (!s.Any(x => char.IsLetter(x)) && uint.TryParse(s, out _))
                            {
                                modsLoaded.Add(s);
                                var mb = modsButtons[s];
                                ModsAvailable.Remove(mb);
                                ModsLoaded.Add(mb);
                            }
                        }
                    }
                }
            }
        }
        else
            fcd.Destroy();
    }
    public void ModsUpdate()
    {
        foreach (var s in modsLoaded)
        {
            var b = modsButtons[s];
            ModsAvailable.Remove(b);
            ModsLoaded.Add(b);
        }
    }

    public Process server;
    protected void MainStart(object sender, EventArgs e)
    {
        if (modsLoaded.Count > 0 && !Directory.Exists(System.IO.Path.Combine(data.modsPath, modsLoaded[0])))
        {
            if (data.modsPath == "")
                new MessageDialog(this, DialogFlags.DestroyWithParent, MessageType.Error, ButtonsType.None, "The mods path is empty. Assign it an empty folder!", this).Show();
            else
                new MessageDialog(this, DialogFlags.DestroyWithParent, MessageType.Error, ButtonsType.None, "Mods have not yet been installed into the mods path.", this).Show();
            return;
        }

        if (server != null && !server.HasExited)
        {
            var md = new MessageDialog(this, DialogFlags.DestroyWithParent, MessageType.Info, ButtonsType.YesNo, "Are you sure you wanna stop the server?", this);
            ResponseType response = (ResponseType)md.Run();
            if (response == ResponseType.Yes)
            {
                ((Button)sender).Label = "Start Server";

                server.CloseMainWindow();
                server.Close();
                server.Dispose();
                server = null;

                md.Destroy();
            }
            else
                md.Destroy();
        }
        else
        {
            ((Button)sender).Label = "Stop Server";

            server = new Process();

            server.StartInfo.WorkingDirectory = data.serverPath;
            server.StartInfo.FileName = $"{data.serverPath}/arma3server_x64";
            server.StartInfo.Arguments = "-config=server.cfg";
            if (modsLoaded.Count > 0)
            {
                var splendidSplit = data.modsPath.Split(new[] { data.serverPath + "/" }, StringSplitOptions.None);
                string modsRelative = splendidSplit[splendidSplit.Length - 1];

                if (splendidSplit.Length <= 1)
                    new MessageDialog(this, DialogFlags.DestroyWithParent, MessageType.Error, ButtonsType.None, "The mods path is outside the server folder! This may lead to some mods not being loaded if the paths are too long", this).Show();

                string modArgs = $" -mod={System.IO.Path.Combine(modsRelative, modsLoaded[0])}";
                for (int i = 1; i < modsLoaded.Count; i++)
                    modArgs += $"\\;{System.IO.Path.Combine(modsRelative, modsLoaded[i])}";
                server.StartInfo.Arguments += modArgs;
            }
            COut(server.StartInfo.FileName + " " + server.StartInfo.Arguments + "\n");

            server.StartInfo.UseShellExecute = false;
            server.StartInfo.RedirectStandardError = true;
            server.StartInfo.RedirectStandardOutput = true;

            server.EnableRaisingEvents = true;
            server.OutputDataReceived += ServerOutputDataReceived;
            server.ErrorDataReceived += ServerErrorDataReceived;
            server.Exited += ServerExited;

            server.Start();
            server.BeginErrorReadLine();
            server.BeginOutputReadLine();
        }
    }
    void ServerExited(object sender, EventArgs e)
    {
        COut(string.Format("\nServer exited with code {0}\n\n", server.ExitCode));
    }
    void ServerErrorDataReceived(object sender, DataReceivedEventArgs e)
    {
        COut(e.Data);
    }
    void ServerOutputDataReceived(object sender, DataReceivedEventArgs e)
    {
        COut(e.Data);
    }

    public string guisco = "hi :)";
    public TextView guisc
    {
        get { return GUIServerConsoleOutput; }
    }
    void COut(string n)
    {
        guisco += $"{n}\n";
        /*Application.Invoke(delegate
        {
            GUIServerConsoleOutput.Buffer.Text += $"{n}\n";
            if (GUIServerConsoleOutput.Buffer.Text.Length > ushort.MaxValue)
                GUIServerConsoleOutput.Buffer.Text = GUIServerConsoleOutput.Buffer.Text.Remove(0, ushort.MaxValue / 2);
            else
            {
                var v = ((ScrolledWindow)GUIServerConsoleOutput.Parent).Vadjustment;
                v.Value = v.Upper - v.PageSize * 0.5;
            }
        });*/
        Console.WriteLine(n);
    }
}
